
CC = gcc

TARGET = misc
TARGET_PATH = $(HOME)/.develop/C
LIB_PATH = $(TARGET_PATH)/lib
HDR_PATH = $(TARGET_PATH)/include/$(TARGET)

OPTION = -c -O
WARNING = -Wall -std=c11 -pedantic
IFLAG = -I include/ -I $(TARGET_PATH)/include

COMPILE = $(CC) $(OPTION) $(WARNING) $(IFLAG) -L $(LIB_PATH)


$(shell mkdir -p build $(HDR_PATH) $(LIB_PATH))
SRC = $(wildcard src/*.c)


static:
	$(COMPILE) $(SRC)
	ar rv build/lib$(TARGET).a *.o

dynamic:
	$(COMPILE) -fpic $(SRC)
	$(CC) -shared -o build/lib$(TARGET).so *.o

install:
	install build/* $(LIB_PATH)
	install include/* $(HDR_PATH)

clean:
	rm -rf build
	rm *.o

