#ifndef VB_SWAP_H
#define VB_SWAP_H


#include "utils.h"


/*
 *
 */
void vb_iswap(int *a, int *b);

/*
 *
 */
void vb_sswap(short *a, short *b);

/*
 *
 */
void vb_lswap(long *a, long *b);

/*
 *
 */
void vb_dswap(double *a, double *b);

/*
 *
 */
void vb_fswap(float *a, float *b);

/*
 *
 */
void vb_bswap(vb_byte_t *a, vb_byte_t *b);

/*
 *
 */
void vb_swap(void *a, void *b, size_t size);


# ifdef VB_NAMESPACES
struct vb_swap_namespace {
	void (*ints)(int *, int *);
	void (*shorts)(short *, short *);
	void (*longs)(long *, long *);
	void (*doubles)(double *, double *);
	void (*floats)(float *, float *);
	void (*bytes)(vb_byte_t *, vb_byte_t *);
	void (*swap)(void *, void *, size_t);
};

static struct vb_swap_namespace
VB_Swap = {
	.ints     =  &vb_swap_int,
	.shorts   =  &vb_swap_short,
	.longs    =  &vb_swap_long,
	.doubles  =  &vb_swap_double,
	.floats   =  &vb_swap_float,
	.bytes    =  &vb_swap_byte,
	.swap     =  &vb_swap,
};
#endif
#endif /* VB_SWAP_H */

