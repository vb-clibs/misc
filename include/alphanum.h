#ifndef VB_MISC_ALPHANUM_H
#define VB_MISC_ALPHANUM_H

#include <ctype.h>

#include <vb/base.h>


/*
 * return true if string str is a valid integer
 */
bool vb_isnumeric(char* str);

/*
 * converts a hex digit into an integer
 */
vb_byte_t vb_hexdigit2int(char c);

/*
 * converts a hex string number into an integer
 */
vb_ulong_t vb_hex2dec(const char *hex, size_t len);

/*
 * convert an int to a bit string representation
 */
char *vb_int2bitstr(int i, char *word);

/*
 * check if a string is a valid hex number
 */
bool vb_num_ishex(const char *hex, size_t len):

/*
 * check if a char is a valid hex digit
 */
static inline int vb_is_hex_digit(char c) {
	return 	(c >= '0' && c <= '9') ||
			(c >= 'a' && c <= 'f') ||
			(c >= 'A' && c <= 'F');
}

#endif /* VB_MISC_ALPHANUM_H */
