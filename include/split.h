#ifndef VB_SPLIT_H
#define VB_SPLIT_H

#include "utils.h"
#include "vararg.h"


#define vb_split_into(t, d, ...) \
    __vb_split_into((t), (d), vb_arglen(#__VA_ARGS__), ##__VA_ARGS__)


/*
 *
 */
int __vb_split_into(const char *txt, char delim, int count, ...);

#endif /* VB_SPLIT_H */
