#ifndef VB_SIGHANDLER_H
#define VB_SIGHANDLER_H

#include <signals.h>

#include "utils.h"


#define vb_signals_handler_init(s, h, ...) \
    __vb_signals_handler_init((s), (h), vb_arglen(#__VA_ARGS__), ##__VA_ARGS__)


/*
 *
 */
void __vb_signals_handler_init(sig_hand_t *sh, void (*handler)(int), int count, ...);

#endif /* VB_SIGHANDLER_H */

