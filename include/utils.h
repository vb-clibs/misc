#ifndef VB_UTILS_H
#define VB_UTILS_H

#include <math.h>


/*
 * Greatest Common Divisor
 */
int vb_num_gcd(int a, int b);

/*
 * check if number is prime
 */
bool vb_num_isprime(vb_ulong_t n);


#endif /* VB_UTILS_H */
