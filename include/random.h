#ifndef VB_RANDOM_H
#define VB_RANDOM_H


#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

#include "utils.h"


enum vb_charflags {
	VB_CHAR_ALL	    = 0,
	VB_CHAR_NFLAGS  = 4,
	VB_CHAR_SPECIAL = 1 << 1,
	VB_CHAR_NUMBER  = 1 << 2,
	VB_CHAR_UPPER   = 1 << 3,
	VB_CHAR_LOWER   = 1 << 4,
};


/*
 * initialize the seed
 */
void vb_random_init(unsigned int seed);

/*
 * returns a random integer from an input range
 */
int vb_random_integer(int min, int max);

/*
 * returns a random decimal from an input range
 */
float vb_random_decimal(float min, float max);

/*
 * returns a random character
 */
char vb_radom_character(enum vb_charflags flags);

/*
 * returns a random string
 */
char *vb_radom_string(char *str, size_t size, enum vb_charflags flags);


#ifdef VB_NAMESPACES
struct vb_random_namespace {
    void (*init)(unsigned int);
    int (*integer)(int, int);
    float (*decimal)(float, float);
	char (*character)(int);
	char *(*string)(char *, size_t, int);
};

static struct vb_random_namespace
VB_Random = {
    .init       = &vb_random_init,
    .decimal    = &vb_random_decimal,
    .integer    = &vb_random_integer,
	.character  = &vb_random_character,
	.string     = &vb_random_string,
};
#endif
#endif /* VB_RANDOM_H */
