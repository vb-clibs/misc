#include <alphanum.h>


bool vb_isnumeric(char *str)
{
    for(char *tmp = str; *tmp != '\0'; tmp++) {
        if(!isdigit(*tmp)) {
            return false;
        }
    }
    return true;
}


static inline vb_byte_t __vb_hexdigit2int(char c) {
	if(c >= '0' && c <= '9') {
		return c - '0';
	}
	if(c >= 'a' && c <= 'f') {
		return 10 + c - 'a';
	}
	if(c >= 'A' && c <= 'F') {
		return 10 + c - 'A';
	}

	return 0;
}


vb_byte_t vb_hexdigit2int(char c) {
	return __vb_hexdigit2int(c);
}


vb_ulong_t vb_hex2dec(const char *hex, size_t len) {
	vb_byte_t base = 1;
	vb_ulong_t decimal = 0;

	for(size_t i = len-1; i >= 0; i--) {
		decimal += __vb_hexdigit2int(hex[i]) * base;
		base *= 16;
	}

	return decimal;
}


bool vb_num_ishex(const char *hex, size_t len) {
	char *tmp = hex;

	if(len > 2) {
		if(hex[0] == '0' && hex[1] == 'x') {
			tmp = hex + 2;
		}
	}

	for(size_t i = 0; i < len; i++) {
		if(!vb_is_hex_digit(tmp[i])) {
			return false;
		}
	}

	return true;
}


char *vb_int2bitstr(int i, char *word)
{
	int k = 0;
	int bit_len = VB_SIZE_INT * 8;

	for(int b = bit_len-1; b >= 0; b--, k++) {
		word[k] = '0' + !!(i & (1 << b));
	}

	word[k] = '\0';
	return word;
}
