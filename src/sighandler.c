#include <signal.h>


void __vb_signals_handler_init(sig_hand_t *sh, void (*handler)(int), int count, ...)
{
    sh->act.sa_handler = handler;
    sh->act.sa_flags = 0;
    sigemptyset(&sh->act.sa_mask);

    va_list valist;
    va_start(valist, count);

    while(count--) {
        int sig = va_arg(valist, int);
        sigaction(sig, &sh->act, NULL);
        sigaddset(&sh->block, sig);
    }

    va_end(valist);
    sigprocmask(SIG_SETMASK, NULL, &sh->origin);
}
