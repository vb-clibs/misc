#include <utils.h>


int vb_num_gcd(int a, int b)
{
	assert(a >= 0 && b >= 0);

	while(b != 0) {
		int t = a % b;
		a = b;
		b = t;
	}

	return a;
}


bool vb_num_isprime(vb_ulong_t n)
{
	int s = sqrt(n) + 1;

	if(n < 2) {
		return false;
	}

	if(n == 2 || ((n & 1) && n < 9)) {
		return true;
	}

	if(n % 2 == 0) {
		return false;
	}

	for(int i = 3; i <= s; i += 2) {
		if(n % i == 0) {
			return false;
		}
	}

	return true;
}
