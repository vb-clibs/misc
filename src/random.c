#include <random.h>


void vb_random_init(unsigned int seed)
{
    static bool once = false;

    if(!once) {
        seed = (seed == 0) ? (unsigned int)time(NULL) : seed;
        srand(seed);
        once = true;
    }
}


char vb_random_character(enum vb_charflags flags)
{
	int idx = 0;
	int num = 0;
	int start = 33;
	int stop = 126;
	int flag_list[VB_CHAR_NFLAGS] = {VB_CHAR_ALL};

	assert(flags >= 0);

	if(flags == VB_CHAR_ALL) {
		return (char)vb_random_integer(start, stop);
	}

	for(int i = 0; i < VB_CHAR_NFLAGS; i++) {
		if(flags & (1 << i)) {
			flag_list[num] = (1 << i);
			num++;
		}
	}

	if(num > 1) {
		idx = vb_random_integer(0, num-1);
	}

	switch(flag_list[idx]) {
		case VB_CHAR_NUMBER: start = 48; stop = 57;  break;
		case VB_CHAR_UPPER:  start = 65; stop = 90;  break;
		case VB_CHAR_LOWER:  start = 97; stop = 122; break;
		case VB_CHAR_SPECIAL:
			switch(vb_random_integer(1, 4)) {
				case 1: start = 33;  stop = 47;  break;
				case 2: start = 58;  stop = 64;  break;
				case 3: start = 91;  stop = 96;  break;
				case 4: start = 123; stop = 126; break;
			}
		break;
	}

	return (char)vb_random_integer(start, stop);
}


char *vb_random_string(char *str, size_t size, enum vb_charflags flags)
{
	assert(size > 0);
	for(size_t i = 0; i < size; i++) {
		str[i] = vb_random_character(flags);
	}
	return str;
}


int vb_random_integer(int min, int max)
{
    assert(max > min);
	return (rand() % (max - min + 1)) + min;
}


float vb_random_decimal(float min, float max)
{
	assert(max > min);
	return (max - min) * ((float)rand() / RAND_MAX) + min;
}
