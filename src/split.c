#include <split.h>


int __vb_split_into(const char *txt, char delim, int argc, ...)
{
    int ret;
    int *t = NULL;
    int *len = NULL;

    char **arr = NULL;
    char **tmp = NULL;
    char **tokens = NULL;
    char *p = (char *)txt;

    int count = 1;

    while(*p != '\0') {
        if(*p++ == delim) {
            count++;
        }
    }

    t = len = calloc(count, SIZE_INT);

    for(p = (char *)txt; *p != '\0'; p++) {
        *p == delim ? *t++ : (*t)++;
    }

    t = len;
    tokens = arr = malloc(count * SIZE_POINTER);
    p = *arr++ = calloc(*(t++) + 1, SIZE_POINTER);

    while(*txt != '\0') {
        if(*txt == delim) {
            p = *arr++ = calloc(*(t++) + 1, SIZE_POINTER);
            txt++;
        }
        else {
            *p++ = *txt++;
        }
    }

    va_list valist;
    va_start(valist, argc);

    for(int i = 0; i < argc; i++) {
        tmp = (char **)(va_arg(valist, long));
        *tmp = tokens[i];
    }

    ret = count - argc;

    if(ret > 0) {
        for(int i = argc; i < count; i++) {
            free(tokens[i]);
        }
    }
    else if(ret < 0) {
        for(int i = count; i < argc; i++) {
            tmp = (char **)(va_arg(valist, long));
            *tmp = NULL;
        }
    }

    va_end(valist);
    free(len);
    free(tokens);

    return ret;
}
