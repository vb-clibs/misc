#include <swap.h>


void vb_bswap(vb_byte_t *a, vb_byte_t *b)
{
    vb_byte_t t = *a;
    *a = *b;
    *b = t;
}


void vb_iswap(int *a, int *b)
{
    int t = *a;
    *a = *b;
    *b = t;
}


void vb_sswap(short *a, short *b)
{
    short t = *a;
    *a = *b;
    *b = t;
}


void vb_lswap(long *a, long *b)
{
    long t = *a;
    *a = *b;
    *b = t;
}


void vb_dswap(double *a, double *b)
{
    double t = *a;
    *a = *b;
    *b = t;
}


void vb_fswap(float *a, float *b)
{
    float t = *a;
    *a = *b;
    *b = t;
}


void vb_swap(void *a, void *b, size_t size)
{
    vb_byte_t *x = a;
    vb_byte_t *y = b;

    while(size--) {
        vb_swap_byte(x + size, y + size);
    }
}
